/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package konrad.lorenz.edu.co.springboot1.controller;

import konrad.lorenz.edu.co.springboot1.service.PrimerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author LENOVO
 */
@RestController
@RequestMapping(path = "mensajes")
public class PrimerController {
    
    @Autowired
    private PrimerService ps;
    
    @GetMapping(path = {"read/ciudad"})
    public String retornarLeerCiudad() {
        String respuesta = ps.readCiudad();
        return respuesta;
    }
    
    @PostMapping(path = {"create/tipoDocumento"})
    public String retornarCrearTipoDocumento() {
        String respuesta = ps.createTipoDocumento();
        return respuesta;
    }
    
    @DeleteMapping(path = {"delete/persona"})
    public String retornarBorrarPersona() {
        String respuesta = ps.deletePersona();
        return respuesta;
    }
}
