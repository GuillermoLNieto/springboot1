/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package konrad.lorenz.edu.co.springboot1.service;

import org.springframework.stereotype.Service;

/**
 *
 * @author LENOVO
 */
@Service
public class PrimerService {
    
    public String readCiudad() {
        return "Ciudad leeida";
    }
    
    public String createTipoDocumento() {
        return "Se creó un nuevo tipo de documento";
    }
    
    public String deletePersona() {
        return "Persona borrada";
    }
}
